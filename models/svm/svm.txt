
PerformanceVector

PerformanceVector:
accuracy: 75.15%
ConfusionMatrix:
True:	true	false
true:	90	6
false:	77	161
precision: 67.65% (positive class: false)
ConfusionMatrix:
True:	true	false
true:	90	6
false:	77	161
recall: 96.41% (positive class: false)
ConfusionMatrix:
True:	true	false
true:	90	6
false:	77	161
AUC (optimistic): 0.825 (positive class: false)
AUC: 0.825 (positive class: false)
AUC (pessimistic): 0.825 (positive class: false)
